package default_name;

import java.io.*;  

public class Main 
{
	//Method to check if the csv informations are correct
	public static boolean IsInfosCorrect(String[] infos)
	{
		try 
		{
			int height = Integer.parseInt(infos[0]);
			int width = Integer.parseInt(infos[1]);
			int probability = Integer.parseInt(infos[3]);
			
			//Check if height is not below 0
			if(height < 0)
			{
				System.out.println("Height is negative ");
				return false;
			}
			
			//Check if width is not below 0
			if(width < 0)
			{
				System.out.println("width is negative ");
				return false;
			}
			
			//Split the fires Position
			String[] firesPosition = infos[2].split(";");
			
			for(int i = 0; i < firesPosition.length; i++)
			{
				//Get the row and column for one fire
				String[] firePosition = firesPosition[i].split(" ");
				
				int fireRow = Integer.parseInt(firePosition[0]);
				int fireCol = Integer.parseInt(firePosition[1]);
				
				//Check if the row is not out of bounds
				if(fireRow < 0 || fireRow >= height)
				{
					System.out.println("Fire row is out of bounds");
					return false;
				}
				
				//Check if the column is not out of bounds
				if(fireCol < 0 || fireCol >= width)
				{
					System.out.println("Fire column is out of bounds");
					return false;
				}
			}
			
			if(probability < 0 || probability > 100)
			{
				System.out.println("Fire probability is not between 0 and 100");
				return false;
			}
				
		} 
		catch(Exception e)
		{
			 System.out.println("getMessage(): " + e.getMessage());
			 return false;
		}
		
		return true;
	}
	
	public static void main(String[] args) throws IOException, InterruptedException  
	{
		String line = "";  
		
		File file = new File("Fire_infos.csv");		
		BufferedReader br = new BufferedReader(new FileReader(file));  
		
		//First line is column name, we skip it
		br.readLine();
				
		while ((line = br.readLine()) != null)
		{  
			// use comma as separator  
			String[] infos = line.split(",");   
			
			System.out.println(	"Infos [Grid Height = " + infos[0] + 
								", Grid Width = " + infos[1] + 
								", FiresPostion = " +	infos[2] + 
								", Fire Probabilty = " + infos[3] + 
								"]");  
			
			//Check if all data given by the csv are correct
			if(!IsInfosCorrect(infos))
			{
				System.out.println("Error in the csv file");				
				break;
			}
			
			//Create fire manager with height, width and fire probability
			FireManager fireManager = new FireManager(Integer.parseInt(infos[0]),Integer.parseInt(infos[1]), Integer.parseInt(infos[3]));
			
			//Get all fires from csv
			String[] firesPosition = infos[2].split(";");
			
			for(int i = 0; i < firesPosition.length; i++)
			{
				//Get the position from one fire
				String[] firePosition = firesPosition[i].split(" ");
				
				Fire fire = new Fire(Integer.parseInt(firePosition[0]), Integer.parseInt(firePosition[1]));
				
				//Add the starting fire
				fireManager.AddFire(fire);	
			}
			
			//Run the simulation until there is no fire
			while(fireManager.HasFire())
			{
				//Wait 1 sec between each fire
				Thread.sleep(1000);
				fireManager.Update();
			}
			
			 System.out.println("Simulation Done");
		}  
		
		br.close();
	}
}