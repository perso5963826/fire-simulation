package default_name;

import javax.swing.JPanel;

public class Square 
{
	public JPanel panel;
	public ForestState state;
	
	public Square()
	{
		panel = new JPanel();
		state = ForestState.CALM;
	}	
}