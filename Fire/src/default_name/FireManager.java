package default_name;

import java.awt.*;
import java.lang.Math;
import java.util.Vector;

import javax.swing.*;

public class FireManager
{
	private JFrame frame;
	private int height;
	private int width;
	private Square[][] board; 
	private Vector<Fire> fires;  
	private int fireSpreadingProbability;
	
	//Constructor
    public FireManager(int height, int width, int fireSpreadingProbability) 
    {
    	frame = new JFrame();
        frame.setTitle("Fire simulation");
        frame.setSize(800, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(height, width));
        
        this.height = height;
        this.width = width;
        this.fireSpreadingProbability = fireSpreadingProbability;
        
        fires = new Vector<Fire>();
        
        //Creating the square of the board
        createBoard();
        frame.setVisible(true);
    }
 
    private void createBoard() 
    {
        board = new Square[height][width];
        
        for (int row = 0; row < height; row++) 
        {
            for (int col = 0; col < width; col++) 
            {
                JPanel panel = new JPanel();
                //Delimit the square by a black border
                panel.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
                //Putting white color for the square
                panel.setBackground(new Color(0, 255, 0)); 
                frame.add(panel);
                
                board[row][col] = new Square();
                board[row][col].panel = panel;               
            }
        }
    }
        
    public void AddFire(Fire fire)
    {
    	//Change the color to red when burning
    	board[fire.row][fire.column].panel.setBackground(new Color(255, 0, 0));
    	//Change the state of the square to burning
    	board[fire.row][fire.column].state = ForestState.BURNING;
    	fires.add(fire);
    }
    
    private void CleanFire(Fire fire)
    {
    	//Change the color to grey when burned
		board[fire.row][fire.column].panel.setBackground(new Color(163, 163, 163));
		//Change the state of the square to burned
		board[fire.row][fire.column].state = ForestState.BURNED;
    }
    
    public void Update()
    {  	  	
    	//Vector to store potential new fires
    	Vector<Fire> newFires = new Vector<Fire>();
    	
    	//Check all fires currently burning
    	for (int i = 0; i < fires.size(); i++)
    	{
    		Fire currentFire = fires.get(i);
    		
    		//Check if the square above is correct
    		if(currentFire.row - 1 >= 0 && board[currentFire.row - 1][currentFire.column].state == ForestState.CALM)
    		{
    			//If true, there is a new fire
    			if((int)((Math.random() * 100) + 1) <= fireSpreadingProbability)
    				newFires.add(new Fire(currentFire.row - 1, currentFire.column));
    		}
    		
    		//Check if the square below is correct
    		if(currentFire.row + 1 < height && board[currentFire.row + 1][currentFire.column].state == ForestState.CALM)
    		{
    			//If true, there is a new fire
    			if((int)((Math.random() * 100) + 1) <= fireSpreadingProbability)
    				newFires.add(new Fire(currentFire.row + 1, currentFire.column));
    		}
    		
    		//Check if the left square is correct
    		if(currentFire.column - 1 >= 0 && board[currentFire.row][currentFire.column - 1].state == ForestState.CALM)
    		{
    			//If true, there is a new fire
    			if((int)((Math.random() * 100) + 1) <= fireSpreadingProbability)
    				newFires.add(new Fire(currentFire.row, currentFire.column - 1));
    		}
    		
    		//Check if the right square is correct
    		if(currentFire.column + 1 < width && board[currentFire.row][currentFire.column + 1].state == ForestState.CALM)
    		{
    			//If true, there is a new fire
    			if((int)((Math.random() * 100) + 1) <= fireSpreadingProbability)
    				newFires.add(new Fire(currentFire.row, currentFire.column + 1));
    		}
    		
    		CleanFire(currentFire);
    	}	
    	
    	fires.clear();
    	
    	for (int i = 0; i < newFires.size(); i++)
    	{
    		AddFire(newFires.get(i));
    	}
    
    	newFires.clear();
    }
    
    public boolean HasFire()
    {   	
    	//Check if there is still fires
    	return fires.size() > 0;   	
    }
}
