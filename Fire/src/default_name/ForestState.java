package default_name;

public enum ForestState 
{
	BURNING, BURNED, CALM
}
